package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;

public class FrameTest {

	@Test
	public void testGetFirstThrow() throws Exception {
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(2, frame.getFirstThrow());
	}

	@Test
	public void testGetSecondThrow() throws Exception {
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(4, frame.getSecondThrow());
	}

	@Test
	public void testGetScore() throws Exception {
		int firstThrow = 2;
		int secondThrow = 6;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(8, frame.getScore());
	}

	@Test
	public void FrameShouldBeSpare() throws Exception {
		int firstThrow = 1;
		int secondThrow = 9;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertTrue(frame.isSpare());
	}

	@Test
	public void FrameShouldNotBeSpare() throws Exception {
		int firstThrow = 1;
		int secondThrow = 6;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertFalse(frame.isSpare());
	}

	@Test
	public void testGetScoreWithSpareBonus() throws Exception {
		int firstThrow = 1;
		int secondThrow = 9;
		Frame frame = new Frame(firstThrow, secondThrow);
		frame.setBonus(3);
		assertEquals(13, frame.getScore());
	}
	
	@Test
	public void FrameShouldBeStrike() throws Exception {
		int firstThrow = 10;
		int secondThrow = 0;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void FrameShouldNotBeStrike() throws Exception {
		int firstThrow = 2;
		int secondThrow = 6;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertFalse(frame.isStrike());
	}
	
	@Test
	public void testGetScoreWithStrinkeBonus() throws Exception {
		int firstThrow = 10;
		int secondThrow = 0;
		Frame frame = new Frame(firstThrow, secondThrow);
		frame.setBonus(9);
		assertEquals(19, frame.getScore());
	}

}
