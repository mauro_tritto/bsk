package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	public static final int GAMELENGTH = 10;

	@Test
	public void testAddOneFrameToEmptyGame() throws Exception {
		Game game = new Game();
		Frame frame = new Frame(2, 4);
		game.addFrame(frame);
		assertEquals(frame, game.getFrameAt(0));
	}

	@Test
	public void testCalculateScoreOnFullGame() throws Exception {
		int[][] arrayFrames = { { 1, 5 }, { 3, 6 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 6 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		assertEquals(81, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithSpare() throws Exception {
		int[][] arrayFrames = { { 1, 9 }, { 3, 6 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 6 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		assertEquals(88, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithStrike() throws Exception {
		int[][] arrayFrames = { { 10, 0 }, { 3, 6 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 6 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		assertEquals(94, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithSpareAndStrike() throws Exception {
		int[][] arrayFrames = { { 10, 0 }, { 4, 6 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 6 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		assertEquals(103, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithMultipleStrikes() throws Exception {
		int[][] arrayFrames = { { 10, 0 }, { 10, 0 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 6 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		assertEquals(112, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithMultipleSpares() throws Exception {
		int[][] arrayFrames = { { 8, 2 }, { 5, 5 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 6 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		assertEquals(98, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithSpareAsTheLastFrame() throws Exception {
		int[][] arrayFrames = { { 1, 5 }, { 3, 6 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 2, 8 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		game.setFirstBonusThrow(7);
		assertEquals(90, game.calculateScore());
	}

	@Test
	public void testCalculateGameScoreWithStrikeAsTheLastFrame() throws Exception {
		int[][] arrayFrames = { { 1, 5 }, { 3, 6 }, { 7, 2 }, { 3, 6 }, { 4, 4 }, { 5, 3 }, { 3, 3 }, { 4, 5 },
				{ 8, 1 }, { 10, 0 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92, game.calculateScore());
	}

	@Test
	public void testCalculateGameBestScore() throws Exception {
		int[][] arrayFrames = { { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 },
				{ 10, 0 }, { 10, 0 } };

		Game game = new Game();
		for (int i = 0; i < GAMELENGTH; i++) {
			Frame frame = new Frame(arrayFrames[i][0], arrayFrames[i][1]);
			game.addFrame(frame);
		}
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300, game.calculateScore());
	}

}
