package tdd.training.bsk;

public class Game {

	public static final int GAMELENGTH = 10;

	private Frame[] game;
	private int indexFreeFrame;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.game = new Frame[GAMELENGTH];
		this.indexFreeFrame = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.game[this.indexFreeFrame++] = frame;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return game[index];
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int acc = 0;
		for (int i = 0; i < GAMELENGTH; i++) {
			acc += this.game[i].getFirstThrow();
			acc += this.game[i].getSecondThrow();
			if (this.game[i].isStrike()) {
				if (i == GAMELENGTH - 1) {
					acc += this.getFirstBonusThrow();
					acc += this.getSecondBonusThrow();
				} else if (this.game[i + 1].isStrike()) {
					if (i == GAMELENGTH - 2) {
						acc += this.game[i + 1].getFirstThrow();
						acc += this.getFirstBonusThrow();
					} else {
						acc += this.game[i + 1].getFirstThrow();
						acc += this.game[i + 2].getFirstThrow();
					}
				} else {
					acc += this.game[i + 1].getFirstThrow();
					acc += this.game[i + 1].getSecondThrow();
				}
			} else if (this.game[i].isSpare()) {
				if (i == GAMELENGTH - 1) {
					acc += this.getFirstBonusThrow();
				} else {
					acc += this.game[i + 1].getFirstThrow();
				}
			}
		}
		return acc;
	}

}
